'use strict';
const users = require('../db/seeds/users.json')
const UserModel = require('../models/user');
class UserDAO {
  /**
   * Instead of establishing real database connection and
   * simplify code example we just getting user data from
   * local database seed
   */
  getUserById(id) {
    return new UserModel(users.find(el => el.id === id));
  }
}

module.exports = UserDAO;