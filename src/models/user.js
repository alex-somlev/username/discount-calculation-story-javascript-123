'use strict';

const Order = require('./order');

const FIVE_PERCENT_DISCOUNT = 0.05;
const TEN_PERCENT_DISCOUNT = 0.1;
const FIFTEEN_PERCENT_DISCOUNT = 0.15;
const DISCOUNT_LEVEL1_THRESHOLD_USD = 1000;
const DISCOUNT_LEVEL2_THRESHOLD_USD = 3000;
const DISCOUNT_LEVEL3_THRESHOLD_USD = 5000;
class User {
  constructor(data = {}) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.email = data.email;
    this.signUpDate = data.signUpDate;
    this.lastLoginDate = data.lastLoginDate;
    this.refererId = data.refererId;
    this.partnerCode = data.partnerCode;
    this.firebaseCloudMessagingToken = data.firebaseCloudMessagingToken;
    this.setOrders(data.orders);
    this.discountThresholdPercentageMap = {
      [FIVE_PERCENT_DISCOUNT]: DISCOUNT_LEVEL1_THRESHOLD_USD,
      [TEN_PERCENT_DISCOUNT]: DISCOUNT_LEVEL2_THRESHOLD_USD,
      [FIFTEEN_PERCENT_DISCOUNT]: DISCOUNT_LEVEL3_THRESHOLD_USD
    }
  }

  getId() {
    return this.id;
  }

  setId(id) {
    this.id = id;
  }

  getFirstName() {
    return this.firstName;
  }

  setFirstName(firstName) {
    this.firstName = firstName;
  }

  getLastName() {
    return this.lastName;
  }

  setLastName(lastName) {
    this.lastName = lastName;
  }

  getEmail() {
    return this.email;
  }

  setEmail(email) {
    this.email = email;
  }

  getOrders() {
    return this.orders;
  }

  setOrders(orders) {
    this.orders = orders.map(order => new Order(order));
  }

  getSignUpDate() {
    return this.signUpDate;
  }

  setSignUpDate(signUpDate) {
    this.signUpDate = signUpDate;
  }

  getLastLoginDate() {
    return this.lastLoginDate;
  }

  setLastLoginDate(lastLoginDate) {
    this.lastLoginDate = lastLoginDate;
  }

  getRefererId() {
    return this.refererId;
  }

  setRefererId(refererId) {
    this.refererId = refererId;
  }

  getPartnerCode() {
    return this.partnerCode;
  }

  setPartnerCode(partnerCode) {
    this.partnerCode = partnerCode;
  }

  getFirebaseCloudMessagingToken() {
    return this.firebaseCloudMessagingToken;
  }

  setFirebaseCloudMessagingToken(firebaseCloudMessagingToken) {
    this.firebaseCloudMessagingToken = firebaseCloudMessagingToken;
  }

  calculateDiscountForUser() {
    const totalPriceOfAllOrders = this.calculateTotalPriceOfAllOrders();
    for (const key of Object.keys(this.discountThresholdPercentageMap).reverse()) {
      if (totalPriceOfAllOrders >= this.discountThresholdPercentageMap[key]) {
        return Number(key);
      }
    }
    return 0;
  }

  calculateTotalPriceOfAllOrders() {
    return this.orders
      .map(order => order.getOrderPrice() + order.getTotalTax())
      .reduce((totalPrice, orderPrice) => totalPrice + orderPrice, 0)
  }
}

module.exports = User;